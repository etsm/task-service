package com.etsm.taskservice;

import DTO.ListIdsDTO;
import DTO.TaskDTO;
import com.etsm.taskservice.controller.mutation.TaskMutations;
import com.etsm.taskservice.controller.query.TaskQuery;
import com.etsm.taskservice.exceptions.WorkspaceNotFound;
import com.etsm.taskservice.model.Workspace;
import com.etsm.taskservice.repository.WorkspaceRepository;
import com.etsm.taskservice.service.task.TaskService;
import com.etsm.taskservice.service.worksapce.WorkspaceService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import utils.ETaskStatus;

import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class TaskServiceTest {

    private Workspace workspace = null;

    @Autowired
    private TaskMutations taskMutations;

    @Autowired
    TaskQuery taskQuery;

    @Autowired
    WorkspaceRepository workspaceRepository;

    @Autowired
    WorkspaceService workspaceService;

    @Autowired
    TaskService taskService;

    @Test
    void contextLoads() {
        assertThat(this.taskMutations).isNotNull();
        assertThat(this.workspaceRepository).isNotNull();
        assertThat(this.taskQuery).isNotNull();
        assertThat(this.taskService).isNotNull();
        assertThat(this.workspaceService).isNotNull();
    }

    @BeforeAll
    void createWorkspace() {
        workspace = new Workspace();
        workspace.setName("TestWorkspace");
        workspace.setIdentifier("TW");
        workspaceRepository.save(workspace);

    }

    @AfterAll
    void clearWorkspaces() {
       workspaceRepository.deleteAll();
    }

    @Test
    void notFoundTestWorkspace() {
        TaskDTO taskDTO = TaskDTO.builder()
                .title("task")
                .status(ETaskStatus.READY_FOR_WORK)
                .createTime(LocalDateTime.now())
                .build();

        assertThatExceptionOfType(WorkspaceNotFound.class)
                .isThrownBy(() -> this.taskMutations.createTask(taskDTO));
    }

    private TaskDTO createTask(TaskDTO taskDto) throws Exception {
        return this.taskMutations.createTask(taskDto);
    }


    @Test
    @Transactional
    void updateTaskTest()  throws Exception{
        TaskDTO taskDTO = TaskDTO.builder()
                .title("task")
                .status(ETaskStatus.READY_FOR_WORK)
                .createTime(LocalDateTime.now())
                .workspace(workspaceService.convert(workspace))
                .build();

        TaskDTO createdTask = this.createTask(taskDTO);
        TaskDTO blockedTask = TaskDTO.builder().title("Blocked task")
                .status(ETaskStatus.READY_FOR_WORK)
                .createTime(LocalDateTime.now())
                .workspace(workspaceService.convert(workspace))
                .build();
        TaskDTO createdBlockedTask = this.createTask(blockedTask);

        assertThat(createdBlockedTask.getId()).isNotNull();
        assertThat(createdBlockedTask.getTaskRelations()).isEmpty();
        assertThat(createdTask.getId()).isNotNull();
        assertThat(createdTask.getTaskRelations()).isEmpty();
        List<Long> ids = new ArrayList<>();
        ids.add(createdBlockedTask.getId());
        ListIdsDTO listIdsDTO = new ListIdsDTO(ids);
        TaskDTO updateCandidate = taskMutations.updateBlocksListTask(createdTask.getId().toString(), listIdsDTO);
        updateCandidate.setTitle("Updated task");

        TaskDTO updatedTask = taskMutations.updateTask(updateCandidate);
        TaskDTO relatedTask = taskQuery.getTask(createdBlockedTask.getId().toString());

        assertThat(updatedTask.getTitle()).isEqualTo("Updated task");
        assertThat(updatedTask.getTaskRelations()).isNotEmpty();
        assertThat(relatedTask.getTaskRelations()).isNotEmpty();
    }

}
