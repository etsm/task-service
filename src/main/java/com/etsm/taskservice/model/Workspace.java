package com.etsm.taskservice.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Workspace {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "department_id")
    private Long departmentId;

    @Column(name = "task_counter", columnDefinition = "bigint default 1")
    private Long taskCounter = 1L;

    @Column(nullable = false, unique = true)
    private String identifier;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "workspace", fetch = FetchType.EAGER)
    private Set<Task> taskList;

    @Column
    private String description;

    public Workspace() {
        this.taskList = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTaskCounter() {
        return taskCounter;
    }

    public void setTaskCounter(Long taskCounter) {
        this.taskCounter = taskCounter;
    }

    public void incrementTaskCounter() {
        if (this.taskCounter == null) {
            this.taskCounter = 1L;
        } else {
            this.taskCounter++;
        }

    }

    public Set<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(Set<Task> taskList) {
        if (taskList != null) {
            this.taskList = taskList;
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Workspace)) return false;
        Workspace workspace = (Workspace) o;
        return Objects.equals(id, workspace.id) && Objects.equals(departmentId, workspace.departmentId) && Objects.equals(taskCounter, workspace.taskCounter) && Objects.equals(identifier, workspace.identifier) && Objects.equals(name, workspace.name) && Objects.equals(description, workspace.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, departmentId, taskCounter, identifier, name, description);
    }
}
