package com.etsm.taskservice.model;

import utils.ERelationType;

import javax.persistence.*;

@Entity(name = "task_relation")
public class TaskRelation implements Comparable<TaskRelation> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "task_id", nullable = false)
    private Task task;

    @Column(nullable = false)
    private ERelationType type;


    public TaskRelation(Long id, Task task, ERelationType type) {
        this.id = id;
        this.task = task;
        this.type = type;
    }

    public TaskRelation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public ERelationType getType() {
        return type;
    }

    public void setType(ERelationType type) {
        this.type = type;
    }

    @Override
    public int compareTo(TaskRelation taskRelation) {
        if (taskRelation == null || id == null) {
            return -1;
        }

        return id.compareTo(taskRelation.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskRelation that = (TaskRelation) o;

        if (!id.equals(that.id)) return false;
        if (!task.equals(that.task)) return false;
        return type == that.type;
    }

    @Override
    public int hashCode() {
        int result = 0;
        if (id != null) {
            result = id.hashCode();
        }
        result = 31 * result + task.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }
}
