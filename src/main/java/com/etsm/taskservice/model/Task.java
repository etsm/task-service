package com.etsm.taskservice.model;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import utils.ETaskStatus;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

@Entity(name = "task")
public class Task implements Comparable<Task>{

    /**
     * id задачи
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * id исполнителя
     */
    @Column(name = "executor_id")
    private Long executorId;


    /**
     * id автора
     */
    @Column(name = "author_id")
    private Long authorId;

    /**
     * пространство,которому принадлежит задача.
     */
    @ManyToOne(cascade = CascadeType.MERGE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Workspace workspace;

    /**
     * идентификатор задачи
     */
    @Column(name = "task_identifier", unique = true)
    private String taskIdentifier;

    /**
     * Название задачи
     */
    @Column(nullable = false)
    private String title;

    /**
     * статус задачи
     */
    @Column(nullable = false, updatable = true)
    private ETaskStatus status;

    /**
     * описание задачи
     */
    @Column
    private String description;

    /**
     * время создание задачи
     */
    @Column(name = "create_time", nullable = false)
    private LocalDateTime createTime;

    /**
     * время первого смены статуса c READY_FOR_WORK на WORK_IN_PROGRESS
     */
    @Column(name = "start_time")
    private LocalDateTime startTime;

    /**
     * время смены статуса c WORK_IN_PROGRESS на RESOLVE
     */
    @Column(name = "end_time")
    private LocalDateTime endTime;

    /**
     * первичная оценка времения выполнения задачи
     */
    @Column(name = "initial_estimate_of_task_execution_time")
    private Duration initialEstimateOfTaskExecutionTime;

    /**
     * затраенное время на выполнение задачи
     */
    @Column(name = "elapsed_task_execution_time")
    private Duration elapsedTaskExecutionTime;

    /**
     * оставшееся время выполнения задачи
     */
    @Column(name = "time_remaining_for_the_task")
    private Duration timeRemainingForTheTask;

    @OneToMany
    @JoinTable(name = "relation", joinColumns = @JoinColumn(name = "task_id"), inverseJoinColumns = @JoinColumn(name = "relation_id"))
    private Set<TaskRelation> taskRelations = new TreeSet<>();

    public Task(Long id,
                Long executorId,
                Long authorId,
                Workspace workspace,
                String taskIdentifier,
                String title,
                ETaskStatus status,
                String description,
                LocalDateTime createTime,
                LocalDateTime startTime,
                LocalDateTime endTime,
                Duration initialEstimateOfTaskExecutionTime,
                Duration elapsedTaskExecutionTime,
                Duration timeRemainingForTheTask,
                Set<TaskRelation> taskRelations
                ) {
        this.id = id;
        this.executorId = executorId;
        this.authorId = authorId;
        this.workspace = workspace;
        this.taskIdentifier = taskIdentifier;
        this.title = title;
        this.status = status;
        this.description = description;
        this.createTime = createTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.initialEstimateOfTaskExecutionTime = initialEstimateOfTaskExecutionTime;
        this.elapsedTaskExecutionTime = elapsedTaskExecutionTime;
        this.timeRemainingForTheTask = timeRemainingForTheTask;
        this.taskRelations = taskRelations;
    }

    public Task() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExecutorId() {
        return executorId;
    }

    public void setExecutorId(Long executorId) {
        this.executorId = executorId;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getTaskIdentifier() {
        return taskIdentifier;
    }

    public void setTaskIdentifier(String taskIdentifier) {
        this.taskIdentifier = taskIdentifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ETaskStatus getStatus() {
        return status;
    }

    public void setStatus(ETaskStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public Duration getInitialEstimateOfTaskExecutionTime() {
        return initialEstimateOfTaskExecutionTime;
    }

    public void setInitialEstimateOfTaskExecutionTime(Duration initialEstimateOfTaskExecutionTime) {
        this.initialEstimateOfTaskExecutionTime = initialEstimateOfTaskExecutionTime;
    }

    public Duration getElapsedTaskExecutionTime() {
        return elapsedTaskExecutionTime;
    }

    public void setElapsedTaskExecutionTime(Duration elapsedTaskExecutionTime) {
        this.elapsedTaskExecutionTime = elapsedTaskExecutionTime;
    }

    public Duration getTimeRemainingForTheTask() {
        return timeRemainingForTheTask;
    }

    public void setTimeRemainingForTheTask(Duration timeRemainingForTheTask) {
        this.timeRemainingForTheTask = timeRemainingForTheTask;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }

    public Set<TaskRelation> getTaskRelations() {
        return taskRelations;
    }

    public void setTaskRelations(Set<TaskRelation> taskRelations) {
        this.taskRelations = taskRelations;
    }

    public void addRelation(TaskRelation taskRelationCandidate) {
        taskRelations.add(taskRelationCandidate);
    }

    @Override
    public int compareTo(Task task) {
        return id.compareTo(task.id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;
        Task task = (Task) o;
        return Objects.equals(id, task.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
