package com.etsm.taskservice.controller.mutation;

import DTO.DepartmentDTO;
import DTO.EmployeeDTO;
import DTO.ListIdsDTO;
import DTO.WorkspaceDTO;
import com.etsm.taskservice.service.worksapce.WorkspaceService;
import exceptions.NotFoundException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/mutation/workspace")
public class WorkspaceMutations {
    private final WorkspaceService workspaceService;

    public WorkspaceMutations(WorkspaceService workspaceService) {
        this.workspaceService = workspaceService;
    }

    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public WorkspaceDTO createWorkspace(@RequestBody WorkspaceDTO workspaceDTO){
        return this.workspaceService.createWorkspace(workspaceDTO);
    }

    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public WorkspaceDTO updateWorkspace(@RequestBody WorkspaceDTO workspaceDTO) throws NotFoundException {
        return this.workspaceService.updateWorkspace(workspaceDTO);
    }

    @DeleteMapping(value = "/remove")
    public Boolean remove(@RequestBody ListIdsDTO listIds) {
        return this.workspaceService.remove(listIds.getIds());
    }

    @PutMapping(value = "/department-was-deleted", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Boolean synchronize(@RequestBody DepartmentDTO departmentDTO) {
        List<WorkspaceDTO> workspaces = this.workspaceService.getWorkspaceListByDepartmentId(departmentDTO.getId());
        List<Long> ids = workspaces.stream().map(WorkspaceDTO::getId).collect(Collectors.toList());

        return this.workspaceService.remove(ids);
    }
}
