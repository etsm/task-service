package com.etsm.taskservice.controller.mutation;

import DTO.EmployeeDTO;
import DTO.ListIdsDTO;
import DTO.TaskDTO;
import com.etsm.taskservice.model.Workspace;
import com.etsm.taskservice.repository.WorkspaceRepository;
import com.etsm.taskservice.service.task.TaskService;
import exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import utils.ERelationType;

import java.util.List;

@RestController
@RequestMapping("/mutation/task")
public class TaskMutations {
    public final TaskService taskService;

    public TaskMutations(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping(value = "/create", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public TaskDTO createTask(@RequestBody TaskDTO taskDTO) throws Exception {
        return this.taskService.createTask(taskDTO);
    }

    @PutMapping(value = "/update", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public TaskDTO updateTask(@RequestBody TaskDTO taskDTO) throws NotFoundException {
        return this.taskService.updateTask(taskDTO);
    }

    @PutMapping(value = "/update-blocks-list/{targetId}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public TaskDTO updateBlocksListTask(@PathVariable String targetId, @RequestBody ListIdsDTO ids) throws Exception {
        return this.taskService.updateTaskRelations(Long.parseLong(targetId), ids.getIds(), ERelationType.BLOCK, ERelationType.BLOCKED_BY);
    }

    @PutMapping(value = "/update-blocked-list/{targetId}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public TaskDTO updateBlockedListTask(@PathVariable String targetId, @RequestBody ListIdsDTO ids) throws Exception {
        return this.taskService.updateTaskRelations(Long.parseLong(targetId), ids.getIds(), ERelationType.BLOCKED_BY, ERelationType.BLOCK);
    }

    @DeleteMapping("/remove")
    public Boolean remove(@RequestBody ListIdsDTO idList) {
        return this.taskService.remove(idList.getIds());
    }

    @PutMapping(value = "/employee-was-deleted", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Boolean synchronize(@RequestBody List<EmployeeDTO> employeeDTO) {
        return this.taskService.synchronizeTasksByRemovedEmployees(employeeDTO);
    }
}
