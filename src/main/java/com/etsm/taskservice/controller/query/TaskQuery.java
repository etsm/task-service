package com.etsm.taskservice.controller.query;


import DTO.TaskDTO;
import DTO.TaskRelationDTO;
import com.etsm.taskservice.service.task.TaskService;
import com.etsm.taskservice.service.taskRelationService.TaskRelationService;
import exceptions.NotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/query/task")
public class TaskQuery {

    private final TaskService taskService;
    private final TaskRelationService taskRelationService;

    public TaskQuery(TaskService taskService, TaskRelationService taskRelationService) {
        this.taskService = taskService;
        this.taskRelationService = taskRelationService;
    }

    @GetMapping("/allBy/author/{id}")
    public List<TaskDTO> getTaskListByAuthorId(@PathVariable String id) {
        return this.taskService.getTaskListByAuthorId(Long.parseLong(id));
    }

    @GetMapping("/allBy/executor/{id}")
    public List<TaskDTO> getTaskListByExecutorId(@PathVariable String id) {
        return this.taskService.getTaskListByExecutorId(Long.parseLong(id));
    }

    @GetMapping("/{id}")
    public TaskDTO getTask(@PathVariable String id) throws NotFoundException {
        return this.taskService.getTask(Long.parseLong(id));
    }

    @GetMapping("/byIdentifier/{identifier}")
    public TaskDTO getTaskByIdentifier(@PathVariable String identifier) throws NotFoundException {
        return this.taskService.getTaskByIdentifier(identifier);
    }

    @GetMapping("/all")
    public List<TaskDTO> getTaskList() {
        return this.taskService.getTaskList();
    }

    @GetMapping("/relatedTasks/{targetId}")
    public Set<TaskRelationDTO> getRelatedTasks(@PathVariable String targetId) throws NotFoundException {
        return taskRelationService.getTaskRelations(Long.parseLong(targetId));
    }

}
