package com.etsm.taskservice.controller.query;


import DTO.TaskDTO;
import DTO.WorkspaceDTO;
import com.etsm.taskservice.service.worksapce.WorkspaceService;
import exceptions.NotFoundException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/query/workspace")
public class WorkspaceQuery {
    private final WorkspaceService workspaceService;

    public WorkspaceQuery(WorkspaceService workspaceService) {
        this.workspaceService = workspaceService;
    }

    @GetMapping("/all")
    public List<WorkspaceDTO> getWorkspaces(){
        return this.workspaceService.getWorkspaceList();
    }

    @GetMapping("/{id}")
    public WorkspaceDTO getWorkspaceById(@PathVariable String id) throws NotFoundException {
        return this.workspaceService.getWorkspaceById(Long.parseLong(id));
    }

    @GetMapping("/{id}/taskList")
    public List<TaskDTO> getTaskListByWorkspace(@PathVariable String id) throws NotFoundException {
        return this.workspaceService.getTaskList(Long.parseLong(id));
    }

    @GetMapping("/byDepartmentId/{departmentId}")
    public List<WorkspaceDTO> getWorkspacesByDepartmentId(@PathVariable String departmentId) {
        return this.workspaceService.getWorkspaceListByDepartmentId(Long.parseLong(departmentId));
    }
}
