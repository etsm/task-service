package com.etsm.taskservice.mapper.workspaceMapper;

import DTO.WorkspaceDTO;
import com.etsm.taskservice.mapper.CycleReferenceResolver;
import com.etsm.taskservice.model.Workspace;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface WorkspaceMapper {
    WorkspaceDTO workspaceToDTO(Workspace workspace, @Context CycleReferenceResolver ctx);

    @InheritInverseConfiguration
    Workspace workspaceDTOToWorkspace(WorkspaceDTO workspaceDTO, @Context CycleReferenceResolver ctx);

    List<Workspace> convertWorkspaceDTOListIntoWorkspaces(List<WorkspaceDTO> workspaces);

    List<WorkspaceDTO> convertWorkspacesIntoWorkspaceDTOList(List<Workspace> workspaces);

    Workspace update(WorkspaceDTO workspaceDTO, @MappingTarget Workspace workspace);
}
