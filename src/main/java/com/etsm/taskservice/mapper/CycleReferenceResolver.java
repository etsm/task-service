package com.etsm.taskservice.mapper;

import org.mapstruct.*;
import java.util.IdentityHashMap;
import java.util.Map;

public class CycleReferenceResolver {
    private final Map<Object, Object> existedReferenceMap = new IdentityHashMap<>();;

    @BeforeMapping
    public <T> T getMappedInstance(Object source, @TargetType Class<T> targetType) {
        return targetType.cast(existedReferenceMap.get(source));
    }

    @BeforeMapping
    public void setObject(Object src, @MappingTarget Object target){
        this.existedReferenceMap.put(src, target);
    }

}
