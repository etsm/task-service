package com.etsm.taskservice.mapper.taskRelationMapper;

import DTO.TaskRelationDTO;
import com.etsm.taskservice.mapper.CycleReferenceResolver;
import com.etsm.taskservice.model.TaskRelation;
import org.mapstruct.Builder;
import org.mapstruct.Context;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface TaskRelationMapper {
    TaskRelationDTO taskRelationToDto(TaskRelation taskRelation, @Context CycleReferenceResolver ctx);

    TaskRelation taskRelationDtoToTaskRelation(TaskRelationDTO taskRelationDTO, @Context CycleReferenceResolver ctx);

    Set<TaskRelationDTO> convertTaskRelationsToDtoSet(Set<TaskRelation> taskRelations);
    Set<TaskRelation> convertTaskRelationDtoSetToTaskRelations(Set<TaskRelationDTO> taskRelations);
}
