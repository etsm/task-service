package com.etsm.taskservice.mapper.taskMapper;

import DTO.TaskDTO;
import com.etsm.taskservice.mapper.CycleReferenceResolver;
import com.etsm.taskservice.model.Task;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", builder = @Builder(disableBuilder = true))
public interface TaskMapper {
    @Mapping(target = "status", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "createTime", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "workspace", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "title", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "taskIdentifier", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    TaskDTO taskToDto(Task task, @Context CycleReferenceResolver ctx);

    @Mappings({
            @Mapping(target = "status", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "createTime", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "workspace", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "title", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
            @Mapping(target = "taskIdentifier", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS),
    })
    @InheritInverseConfiguration
    Task dtoToTask(TaskDTO task, @Context CycleReferenceResolver ctx);

    Task updateTask(TaskDTO taskDTO, @MappingTarget Task task, @Context CycleReferenceResolver ctx);

    List<TaskDTO> convertTasksIntoTaskDTOList(List<Task> tasks);
}
