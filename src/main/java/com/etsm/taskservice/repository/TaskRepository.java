package com.etsm.taskservice.repository;

import com.etsm.taskservice.model.Task;
import com.etsm.taskservice.model.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findAllByAuthorId(Long id);
    List<Task> findAllByExecutorId(Long id);
    List<Task> findAllByWorkspace(Workspace workspace);
    Optional<Task> findById(Long id);
    Optional<Task> findByAuthorId(Long id);
    Optional<Task> findByExecutorId(Long id);
    Optional<Task> findByTaskIdentifier(String identifier);
}
