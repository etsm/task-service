package com.etsm.taskservice.repository;

import com.etsm.taskservice.model.TaskRelation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.Set;

public interface TaskRelationRepository extends JpaRepository<TaskRelation, Long> {
    Set<TaskRelation> findTaskRelationsByTaskId(Long taskId);
}
