package com.etsm.taskservice.repository;

import com.etsm.taskservice.model.Workspace;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface WorkspaceRepository extends JpaRepository<Workspace, Long> {
    Optional<Workspace> findById(Long id);
    Optional<Workspace> findByName(String name);
    List<Workspace> findAllByDepartmentId(Long id);
}

