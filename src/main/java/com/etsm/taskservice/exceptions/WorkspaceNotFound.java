package com.etsm.taskservice.exceptions;

public class WorkspaceNotFound extends Exception{
    public WorkspaceNotFound(){
        super("Workspace not found");
    }
}
