package com.etsm.taskservice.service.worksapce;

import DTO.TaskDTO;
import DTO.WorkspaceDTO;
import com.etsm.taskservice.mapper.CycleReferenceResolver;
import com.etsm.taskservice.mapper.taskMapper.TaskMapper;
import com.etsm.taskservice.mapper.workspaceMapper.WorkspaceMapper;
import com.etsm.taskservice.model.Task;
import com.etsm.taskservice.model.Workspace;
import com.etsm.taskservice.repository.WorkspaceRepository;
import exceptions.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class WorkspaceServiceImpl implements WorkspaceService {
    private final WorkspaceRepository workspaceRepository;
    private final WorkspaceMapper workspaceMapper;
    private final TaskMapper taskMapper;

    public WorkspaceServiceImpl(WorkspaceRepository workspaceRepository, WorkspaceMapper workspaceMapper, TaskMapper taskMapper) {
        this.workspaceRepository = workspaceRepository;
        this.workspaceMapper = workspaceMapper;
        this.taskMapper = taskMapper;
    }

    @Override
    public WorkspaceDTO createWorkspace(WorkspaceDTO workspaceDTO) {
        checkRequiredFields(workspaceDTO);
        Workspace candidateWorkspace = this.workspaceMapper.workspaceDTOToWorkspace(workspaceDTO, new CycleReferenceResolver());
        candidateWorkspace.setTaskCounter(0L);
        Workspace createdWorkspace = this.workspaceRepository.saveAndFlush(candidateWorkspace);
        return this.convert(createdWorkspace);
    }

    @Override
    public WorkspaceDTO updateWorkspace(WorkspaceDTO workspaceDTO) throws NotFoundException {
        checkRequiredFields(workspaceDTO);
        Workspace existWorkspace = this.workspaceRepository.findById(workspaceDTO.getId())
                .orElseThrow(NotFoundException::new);
        Long ownCounter = existWorkspace.getTaskCounter();
        Workspace updateWorkspaceCandidate = this.workspaceMapper.update(workspaceDTO, existWorkspace);
        existWorkspace.setTaskCounter(ownCounter);
        Workspace updatedWorkspace = this.workspaceRepository.saveAndFlush(updateWorkspaceCandidate);
        return this.convert(updatedWorkspace);
    }

    @Override
    public Boolean remove(List<Long> ids) {
        List<Workspace> workspaces = this.workspaceRepository.findAllById(ids);
        this.workspaceRepository.deleteInBatch(workspaces);
        return true;
    }

    @Override
    public WorkspaceDTO getWorkspaceById(Long id) throws NotFoundException {
        Workspace workspace = this.workspaceRepository.findById(id).orElseThrow(NotFoundException::new);
        return this.convert(workspace);
    }

    @Override
    public List<WorkspaceDTO> getWorkspaceList() {
        List<Workspace> workspaces = this.workspaceRepository.findAll();
        return workspaces.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    public List<WorkspaceDTO> getWorkspaceListByDepartmentId(Long id) {
        List<Workspace> workspaces = this.workspaceRepository.findAllByDepartmentId(id);
        return workspaces.stream().map(this::convert).collect(Collectors.toList());
    }

    @Override
    public Boolean notEmptyRepo() {
        return this.workspaceRepository.count() > 0;
    }

    @Override
    public WorkspaceDTO updateCounter(Long id) throws NotFoundException {
        Workspace workspace = this.workspaceRepository.findById(id).orElseThrow(NotFoundException::new);
        workspace.incrementTaskCounter();
        Workspace updatedWorkspace = this.workspaceRepository.saveAndFlush(workspace);
        return this.convert(updatedWorkspace);
    }

    @Override
    public Workspace getWorkspaceEntity(Long id) throws NotFoundException {
        return this.workspaceRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<TaskDTO> getTaskList(Long workspaceId) throws NotFoundException {
        Workspace workspace = this.workspaceRepository.findById(workspaceId).orElseThrow(NotFoundException::new);
        List<Task> taskList = new ArrayList<>(workspace.getTaskList());
        return this.taskMapper.convertTasksIntoTaskDTOList(taskList);
    }

    @Override
    public Workspace convert(WorkspaceDTO workspace) {
        return workspaceMapper.workspaceDTOToWorkspace(workspace, new CycleReferenceResolver());
    }

    public WorkspaceDTO convert(Workspace workspace) {
        return this.workspaceMapper.workspaceToDTO(workspace, new CycleReferenceResolver());
    }

    private void checkRequiredFields(WorkspaceDTO workspaceDTO) {
        String name = workspaceDTO.getName();
        String identifier = workspaceDTO.getIdentifier();
        Long departmentId = workspaceDTO.getDepartmentId();

        if (name.isEmpty() || name.isBlank()) {
            throw new IllegalArgumentException("Argument name is required");
        }

        if (identifier.isEmpty() || identifier.isBlank()) {
            throw new IllegalArgumentException("Argument identifier is required");
        }

        if (departmentId == null) {
            throw new IllegalArgumentException("Argument id is required");
        }
    }
}
