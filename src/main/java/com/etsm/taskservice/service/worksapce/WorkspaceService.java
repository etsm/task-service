package com.etsm.taskservice.service.worksapce;

import DTO.TaskDTO;
import DTO.WorkspaceDTO;
import com.etsm.taskservice.model.Workspace;
import exceptions.NotFoundException;

import java.util.List;

public interface WorkspaceService {
    WorkspaceDTO createWorkspace(WorkspaceDTO workspaceDTO);
    WorkspaceDTO updateWorkspace(WorkspaceDTO workspaceDTO) throws NotFoundException;
    Boolean remove(List<Long> ids);
    WorkspaceDTO getWorkspaceById(Long id) throws NotFoundException;
    Workspace getWorkspaceEntity(Long id) throws NotFoundException;
    List<WorkspaceDTO> getWorkspaceList();
    List<WorkspaceDTO> getWorkspaceListByDepartmentId(Long id);
    Boolean notEmptyRepo();
    WorkspaceDTO updateCounter(Long id) throws NotFoundException;
    List<TaskDTO> getTaskList(Long workspaceId) throws NotFoundException;
    WorkspaceDTO convert(Workspace workspace);
    Workspace convert(WorkspaceDTO workspace);
}
