package com.etsm.taskservice.service.task;

import DTO.EmployeeDTO;
import DTO.TaskDTO;
import DTO.WorkspaceDTO;
import com.etsm.taskservice.model.Task;
import exceptions.NotFoundException;
import utils.ERelationType;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

public interface TaskService {
    TaskDTO createTask(TaskDTO taskDTO) throws Exception;
    TaskDTO updateTask(TaskDTO taskDTO) throws NotFoundException;
    Boolean remove(List<Long> ids);
    List<TaskDTO> getTaskListByAuthorId(Long id);
    List<TaskDTO> getTaskListByExecutorId(Long id);
    List<TaskDTO> getTaskSetByWorkspace(WorkspaceDTO workspaceDTO) throws NotFoundException;
    List<TaskDTO> getTaskList();
    TaskDTO getTask(Long id) throws NotFoundException;
    TaskDTO getTaskByIdentifier(String id) throws NotFoundException;
    Task getTaskEntity(Long id) throws NotFoundException;
    TaskDTO updateTaskRelations(Long taskId, List<Long> ids, ERelationType type, ERelationType reversType) throws  Exception;
    Boolean synchronizeTasksByRemovedEmployees(List<EmployeeDTO> employeeDTOList);
    TaskDTO taskToDto(Task t);
    Task dtoToTask(TaskDTO dto);
}
