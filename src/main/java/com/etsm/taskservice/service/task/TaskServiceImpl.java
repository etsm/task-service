package com.etsm.taskservice.service.task;

import DTO.EmployeeDTO;
import DTO.TaskDTO;
import DTO.WorkspaceDTO;
import com.etsm.taskservice.exceptions.WorkspaceNotFound;
import com.etsm.taskservice.mapper.CycleReferenceResolver;
import com.etsm.taskservice.mapper.taskMapper.TaskMapper;
import com.etsm.taskservice.model.TaskRelation;
import com.etsm.taskservice.model.Task;
import com.etsm.taskservice.model.Workspace;
import com.etsm.taskservice.repository.TaskRelationRepository;
import com.etsm.taskservice.repository.TaskRepository;
import com.etsm.taskservice.repository.WorkspaceRepository;
import com.etsm.taskservice.service.worksapce.WorkspaceService;
import exceptions.NotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import utils.ERelationType;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class TaskServiceImpl implements TaskService{

    private final TaskRepository taskRepository;
    private final WorkspaceRepository workspaceRepository;
    private final WorkspaceService  workspaceService;
    private final TaskMapper taskMapper;
    private final TaskRelationRepository taskRelationRepository;

    public TaskServiceImpl(
            TaskRepository taskRepository,
            WorkspaceRepository workspaceRepository,
            WorkspaceService workspaceService,
            TaskMapper taskMapper,
            TaskRelationRepository taskRelationRepository) {
        this.taskRepository = taskRepository;
        this.workspaceRepository = workspaceRepository;
        this.workspaceService = workspaceService;
        this.taskMapper = taskMapper;
        this.taskRelationRepository = taskRelationRepository;
    }

    @Override
    public List<TaskDTO> getTaskListByAuthorId(Long id) {
        List<Task> tasks = this.taskRepository.findAllByAuthorId(id);
        return tasks.stream().map(this::taskToDto).collect(Collectors.toList());
    }

    @Override
    public List<TaskDTO> getTaskListByExecutorId(Long id) {
        List<Task> tasks = this.taskRepository.findAllByExecutorId(id);
        return tasks.stream().map(this::taskToDto).collect(Collectors.toList());
    }

    @Override
    public TaskDTO createTask(TaskDTO taskDTO) throws Exception {
        if(!this.workspaceService.notEmptyRepo()){
            throw new WorkspaceNotFound();
        }

        WorkspaceDTO workspaceDTO = taskDTO.getWorkspace();

        if(workspaceDTO == null){
            // TODO: написать свое исключение
            throw new Exception("Задача должна принадлежать какому-либо пространству");
        }
        Workspace workspace = this.workspaceRepository.findById(workspaceDTO.getId()).orElseThrow(NotFoundException::new);

        this.workspaceService.updateCounter(workspaceDTO.getId());

        taskDTO.setWorkspace(this.workspaceService.convert(workspace));
        Task task = this.dtoToTask(taskDTO);
        Task createdTask = this.taskRepository.saveAndFlush(task);
        String identifier = workspace.getIdentifier() + "-" + workspace.getTaskCounter();
        createdTask.setTaskIdentifier(identifier);
        Task updatedIdentifierTask = this.taskRepository.saveAndFlush(createdTask);

        return this.taskToDto(updatedIdentifierTask);
    }

    @Override
    @Transactional
    public TaskDTO updateTask(TaskDTO taskDTO) throws NotFoundException {
        Task existTask = this.taskRepository.findById(taskDTO.getId()).orElseThrow(NotFoundException::new);
        WorkspaceDTO workspaceDTO = workspaceService.getWorkspaceById(taskDTO.getWorkspace().getId());
        taskDTO.setWorkspace(workspaceDTO);
        Task updateTaskCandidate = this.taskMapper.updateTask(taskDTO, existTask, new CycleReferenceResolver());
        Task updatedTask = this.taskRepository.save(updateTaskCandidate);
        return this.taskToDto(updatedTask);
    }

    @Override
    public Boolean remove(List<Long> ids) {
        List<Task> tasks = this.taskRepository.findAllById(ids);
        this.taskRepository.deleteInBatch(tasks);
        return true;
    }

    @Override
    public TaskDTO getTask(Long id) throws NotFoundException {
        Task task = this.taskRepository.findById(id).orElseThrow(NotFoundException::new);
        return this.taskToDto(task);
    }

    @Override
    public List<TaskDTO> getTaskList() {
        List<Task> tasks = this.taskRepository.findAll();
        return tasks.stream().map(this::taskToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public TaskDTO updateTaskRelations(Long taskId, List<Long> ids, ERelationType type, ERelationType reversType) throws SQLIntegrityConstraintViolationException, Exception{
        if(ids.contains(taskId)){
            throw new Exception("Задача не должна блокировать саму себя");
        }

        Task task = this.taskRepository.findById(taskId).orElseThrow(NotFoundException::new);
        List<Task> targetTaskList = taskRepository.findAllById(ids);
        List<TaskRelation> taskRelationList = new ArrayList<TaskRelation>();
        targetTaskList.forEach(candidate -> {
            final TaskRelation taskRelationCandidate = new TaskRelation();
            final TaskRelation reversRelation = new TaskRelation();
            taskRelationCandidate.setTask(candidate);
            taskRelationCandidate.setType(type);
            reversRelation.setTask(task);
            reversRelation.setType(reversType);
            candidate.addRelation(reversRelation);
            task.addRelation(taskRelationCandidate);
            taskRelationList.add(taskRelationCandidate);
            taskRelationList.add(reversRelation);
        });
        taskRelationRepository.saveAll(taskRelationList);
        Task updatedTask = taskRepository.saveAndFlush(task);
        taskRepository.saveAll(targetTaskList);
        return this.taskToDto(updatedTask);
    }

    @Override
    public Boolean synchronizeTasksByRemovedEmployees(List<EmployeeDTO> employeeDTOList) {
        List<Long> targetIds = employeeDTOList.stream().map(EmployeeDTO::getId).collect(Collectors.toList());
        List<Task> taskListResult = new ArrayList<>();

        targetIds.forEach((targetId) -> {
            CompletableFuture<List<Task>> tasksFutureByAuthorId =
                    CompletableFuture.supplyAsync(() ->  this.taskRepository.findAllByAuthorId(targetId));

            CompletableFuture<List<Task>> tasksFutureByExecutorId =
                    CompletableFuture.supplyAsync(() ->  this.taskRepository.findAllByExecutorId(targetId));

            List<Task> tasksByAuthorId = null;
            List<Task> tasksByExecutorId = null;

            try {

                tasksByAuthorId = tasksFutureByAuthorId.get();
                tasksByExecutorId = tasksFutureByExecutorId.get();

                tasksByAuthorId.forEach(task -> {
                    task.setAuthorId(null);
                });

                tasksByExecutorId.forEach(task -> {
                    task.setExecutorId(null);
                });

                taskListResult.addAll(tasksByAuthorId);
                taskListResult.addAll(tasksByExecutorId);

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        });

        this.taskRepository.saveAll(taskListResult);

        return true;
    }

    @Override
    public List<TaskDTO> getTaskSetByWorkspace(WorkspaceDTO workspaceDTO) throws NotFoundException {
        Workspace workspace = this.workspaceService.getWorkspaceEntity(workspaceDTO.getId());
        List<Task> taskList = this.taskRepository.findAllByWorkspace(workspace);
        return taskList.stream().map(this::taskToDto).collect(Collectors.toList());
    }

    @Override
    public Task getTaskEntity(Long id) throws NotFoundException {
        return this.taskRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    private TaskDTO mapToTaskDTO(Task t1){
       return TaskDTO.builder()
                .id(t1.getId())
                .description(t1.getDescription())
                .timeRemainingForTheTask(t1.getTimeRemainingForTheTask())
                .elapsedTaskExecutionTime(t1.getElapsedTaskExecutionTime())
                .initialEstimateOfTaskExecutionTime(t1.getInitialEstimateOfTaskExecutionTime())
                .endTime(t1.getEndTime())
                .startTime(t1.getStartTime())
                .authorId(t1.getAuthorId())
                .executorId(t1.getExecutorId())
                .createTime(t1.getCreateTime())
                .status(t1.getStatus())
                .taskIdentifier(t1.getTaskIdentifier())
                .title(t1.getTitle())
                .workspace(this.workspaceService.convert(t1.getWorkspace()))
                .build();
    }

    @Override
    public TaskDTO taskToDto(Task t) {
        return this.taskMapper.taskToDto(t, new CycleReferenceResolver());
    }


    @Override
    public Task dtoToTask(TaskDTO dto) {
        return this.taskMapper.dtoToTask(dto, new CycleReferenceResolver());
    }

    @Override
    public TaskDTO getTaskByIdentifier(String id) throws NotFoundException {
        Task task = this.taskRepository.findByTaskIdentifier(id).orElseThrow(NotFoundException::new);
        final TaskDTO dto = taskToDto(task);
        return dto;
    }
}
