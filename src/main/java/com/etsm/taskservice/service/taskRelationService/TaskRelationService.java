package com.etsm.taskservice.service.taskRelationService;

import DTO.TaskDTO;
import DTO.TaskRelationDTO;
import com.etsm.taskservice.model.TaskRelation;
import exceptions.NotFoundException;

import java.util.Set;

public interface TaskRelationService {
    Set<TaskRelationDTO> getTaskRelations(Long taskId) throws NotFoundException;
    TaskRelationDTO convert(TaskRelation taskRelation);
    TaskRelation convert(TaskRelationDTO dto);
    Set<TaskRelation> convertTaskRelationDtoSetToTaskRelations(Set<TaskRelationDTO> dtos);
    Set<TaskRelationDTO> convertTaskRelationsToDtoSet(Set<TaskRelation> relations);
}
