package com.etsm.taskservice.service.taskRelationService;

import DTO.TaskDTO;
import DTO.TaskRelationDTO;
import com.etsm.taskservice.mapper.CycleReferenceResolver;
import com.etsm.taskservice.mapper.taskRelationMapper.TaskRelationMapper;
import com.etsm.taskservice.model.TaskRelation;
import com.etsm.taskservice.repository.TaskRelationRepository;
import com.etsm.taskservice.service.task.TaskService;
import exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class TaskRelationServiceImpl implements TaskRelationService {
    private final TaskRelationRepository taskRelationRepository;
    private final TaskRelationMapper taskRelationMapper;
    private final TaskService taskService;

    public TaskRelationServiceImpl(TaskRelationRepository taskRelationRepository, TaskRelationMapper taskRelationMapper, TaskService taskService) {
        this.taskRelationRepository = taskRelationRepository;
        this.taskRelationMapper = taskRelationMapper;
        this.taskService = taskService;
    }

    @Override
    public TaskRelationDTO convert(TaskRelation taskRelation) {
        return taskRelationMapper.taskRelationToDto(taskRelation, new CycleReferenceResolver());
    }

    @Override
    public TaskRelation convert(TaskRelationDTO dto) {
        return taskRelationMapper.taskRelationDtoToTaskRelation(dto, new CycleReferenceResolver());
    }

    @Override
    public Set<TaskRelation> convertTaskRelationDtoSetToTaskRelations(Set<TaskRelationDTO> dtos) {
        return taskRelationMapper.convertTaskRelationDtoSetToTaskRelations(dtos);
    }

    @Override
    public Set<TaskRelationDTO> convertTaskRelationsToDtoSet(Set<TaskRelation> relations) {
        return taskRelationMapper.convertTaskRelationsToDtoSet(relations);
    }

    @Override
    public Set<TaskRelationDTO> getTaskRelations(Long taskId) throws NotFoundException {
        TaskDTO source = taskService.getTask(taskId);
        return source.getTaskRelations();
    }
}
