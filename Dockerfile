#ВАЖНО! Запускать требуется из монорепозитория
FROM openjdk:11

#Создаем папку для библиотеки классов
RUN mkdir /DataTransferObjectLib

#Создаем папку для севиса
RUN mkdir /task-service

#Копируем библиотеку классов в образ
COPY ./DataTransferObjectLib /DataTransferObjectLib/

#Копируем сервис
COPY ./task-service /task-service/

#Устанавливаем рабочую дирректорию
WORKDIR /task-service

#Собираем проект
RUN  ./gradlew clean && ./gradlew build -x test

#Сразу прокидываем нужный порт
EXPOSE 8092

#Создаем дирректорию для запуска
RUN mkdir /app

#Копируем JAR в дирректорию для запуска
RUN cp /task-service/build/libs/*.jar /app/task-service.jar

#запуск сервиса
ENTRYPOINT [\
"java",\
"-XX:+UnlockExperimentalVMOptions",\
"-Djava.security.egd=file:/dev/./urandom",\
"-jar","/app/task-service.jar",\
"--spring.config.location=file:///task-service/src/main/resources/production.yml"]